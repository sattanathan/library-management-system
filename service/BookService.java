package com.netcracker.LMS.service;

import com.netcracker.LMS.dataAccess.entity.Book;
import com.netcracker.LMS.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BookService {

    @Autowired
    BookRepository bookRepository;

    public void addBook(@NotNull Book book) {
        book.setBookId(UUID.randomUUID());
        bookRepository.save(book);
    }

    public List displayAllBooks() {
        return bookRepository.findAll();
    }

    public Optional<Book> findBook(UUID bookId) {
        return bookRepository.findById(bookId);
    }

    public void updateBook(UUID bookId, Book book) {
        book.setBookId(bookId);
        bookRepository.save(book);
    }

    public void deleteBook(UUID bookId) {
        bookRepository.deleteById(bookId);
    }

    public List<Book> findBookByName(String booktitle) {
        return bookRepository.findByName(booktitle);
    }
}
