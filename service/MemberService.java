package com.netcracker.LMS.service;

import com.netcracker.LMS.dataAccess.entity.Member;
import com.netcracker.LMS.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MemberService {

    @Autowired
    MemberRepository memberRepository;

    public void addMember(@NotNull Member member) {
        member.setMemberId(UUID.randomUUID());
        memberRepository.save(member);
    }


    public List<Member> displayAllMember() {
        return memberRepository.findAll();
    }

    public Optional<Member> findMember(UUID id) {
        return memberRepository.findById(id);
    }

    public void updateMember(UUID memberId, Member member) {
        member.setMemberId(memberId);
        memberRepository.save(member);
    }

    public void deleteMember(UUID memberId) {
        memberRepository.deleteById(memberId);
    }

    public List<Member> findMemberByName(String memberName) {
        return memberRepository.findByName(memberName);
    }
}
