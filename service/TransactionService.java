package com.netcracker.LMS.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.LMS.dataAccess.entity.Book;
import com.netcracker.LMS.dataAccess.entity.Member;
import com.netcracker.LMS.dataAccess.entity.Transaction;
import com.netcracker.LMS.dataAccess.entity.models.BookAndMember;
import com.netcracker.LMS.dataAccess.entity.models.BookOfTransaction;
import com.netcracker.LMS.dataAccess.entity.models.MemberOfTransaction;
import com.netcracker.LMS.exceptions.BookNotAvailable;
import com.netcracker.LMS.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    RestTemplate restTemplate;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    Calendar calendar;

    public void addTransaction(Transaction transaction) {
        calendar = Calendar.getInstance();
        if(transactionRepository.findAvailability(transaction.getBookId())){
            transaction.setTransactionId(UUID.randomUUID());
            transaction.setIssueDate(sdf.format(calendar.getTime()));
            Member m = restTemplate.getForObject("http://localhost:8080/MemberDb/findMember?memberId=" + transaction.getMemberId(),  Member.class);
            calendar.add(Calendar.DAY_OF_MONTH,m.getMemberType().getDays());
            transaction.setReturnDate(sdf.format(calendar.getTime()));
            transactionRepository.save(transaction);
        }
        else {
            throw new BookNotAvailable();
        }

    }

    public List<Transaction> displayAllTransaction() {
        return transactionRepository.findAll();
    }

    public Optional<Transaction> findTransaction(UUID transactionId) {
        return transactionRepository.findById(transactionId);
    }

    public void deleteTransaction(UUID transactionId) {
        transactionRepository.deleteById(transactionId);
    }

    public void updateTransaction(UUID transactionId, Transaction transaction) {
        transaction.setTransactionId(transactionId);
        transactionRepository.save(transaction);
    }

    public MemberOfTransaction memberDetails(UUID transactionId) throws JsonProcessingException {
        Optional<Transaction> tran = transactionRepository.findById(transactionId);
        if(tran.isPresent()) {
            UUID memberId = tran.get().getMemberId();
            HttpHeaders header=new HttpHeaders();
            header.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Member> httpEntity=new HttpEntity<>(null,header);
            Member m = restTemplate.getForObject("http://localhost:8080/MemberDb/findMember?memberId=" + memberId,  Member.class);

            MemberOfTransaction trans = new MemberOfTransaction();
            trans.setMemberAddress(m.getMemberAddress());
            trans.setMemberId(m.getMemberId());
            trans.setMemberName(m.getMemberName());
            trans.setMemberType(m.getMemberType());
            trans.setBookId(tran.get().getBookId());
            trans.setTransactionId(transactionId);
            trans.setIssueDate(tran.get().getIssueDate());
            trans.setReturnDate(tran.get().getReturnDate());
            return trans;
        }
        return null;
    }

    public BookOfTransaction bookDetails(UUID transactionId) throws JsonProcessingException {
        Optional<Transaction> tran = transactionRepository.findById(transactionId);
        UUID bookId = tran.get().getBookId();
        String uri="http://localhost:8080/BookDb/findBook";
        UriComponentsBuilder builder= UriComponentsBuilder.fromHttpUrl(uri).queryParam("bookId",bookId);
        Book bk = restTemplate.getForObject(builder.toUriString(), Book.class);

        BookOfTransaction book = new BookOfTransaction();
        book.setAuthor(bk.getAuthor());
        book.setBookId(bk.getBookId());
        book.setBookTitle(bk.getBookTitle());
        book.setISBN(bk.getISBN());
        book.setTransactionId(transactionId);
        book.setMemberId(tran.get().getMemberId());
        book.setIssueDate(tran.get().getIssueDate());
        book.setReturnDate(tran.get().getReturnDate());
        return book;
    }

    public BookAndMember bookAndMemberDetails(UUID transactionId) throws JsonProcessingException{
        Optional<Transaction> tran = transactionRepository.findById(transactionId);
        UUID bookId = tran.get().getBookId();
        String uri="http://localhost:8080/BookDb/findBook";
        UriComponentsBuilder builder= UriComponentsBuilder.fromHttpUrl(uri).queryParam("bookId",bookId);
        Book bk = restTemplate.getForObject(builder.toUriString(), Book.class);

        UUID memberId = tran.get().getMemberId();
        String uri1 = "http://localhost:8080/MemberDb/findMember";
        builder = UriComponentsBuilder.fromHttpUrl(uri1).queryParam("memberId",memberId);
        Member mem= restTemplate.getForObject(builder.toUriString(),Member.class);

        BookAndMember bm = new BookAndMember();
        bm.setAuthor(bk.getAuthor());
        bm.setBookId(bk.getBookId());
        bm.setBookTitle(bk.getBookTitle());
        bm.setISBN(bk.getISBN());
        bm.setTransactionId(transactionId);
        bm.setMemberId(tran.get().getMemberId());
        bm.setIssueDate(tran.get().getIssueDate());
        bm.setReturnDate(tran.get().getReturnDate());
        bm.setMemberAddress(mem.getMemberAddress());
        bm.setMemberId(memberId);
        bm.setMemberName(mem.getMemberName());
        bm.setMemberType(mem.getMemberType());

        return bm;
    }

}
