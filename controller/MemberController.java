package com.netcracker.LMS.controller;

import com.netcracker.LMS.dataAccess.entity.Member;
import com.netcracker.LMS.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequestMapping("/MemberDb")
@RestController
public class MemberController {
    @Autowired
    MemberService memberService;

    @RequestMapping(value = "/addMember",method = RequestMethod.POST)
    public ResponseEntity<HttpStatus> addMember(@RequestBody Member m) {
        HttpStatus status = null;
        try{
            memberService.addMember(m);
            status = HttpStatus.CREATED;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/displayAllMember", method = RequestMethod.GET)
    public ResponseEntity<List> displayAllMember(){
        List<Member> memberList = null;
        HttpStatus status = null;
        try{
            memberList = memberService.displayAllMember();
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(memberList,status);
    }

    @RequestMapping(value = "/findMember", method = RequestMethod.GET)
    public ResponseEntity<Optional<Member>> findMember(@RequestParam UUID memberId){
        HttpStatus status = null;
        Optional<Member> memberList = null;
        try{
            memberList = memberService.findMember(memberId);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(memberList,status);
    }

    @RequestMapping(value = "/updateMember",method = RequestMethod.PUT)
    public ResponseEntity<HttpStatus> updateMember(@RequestParam UUID memberId, @RequestBody Member m){
        HttpStatus status = null;
        try{
            memberService.updateMember(memberId,m);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/deleteMember", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteMember(@RequestParam UUID memberId){
        HttpStatus status = null;
        try{
            memberService.deleteMember(memberId);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/findMemberByName",method = RequestMethod.GET)
    public List<Member> findMemberByName(@RequestParam String memberName){
        return memberService.findMemberByName(memberName);
    }

}
