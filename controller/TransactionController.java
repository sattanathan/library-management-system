package com.netcracker.LMS.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.LMS.dataAccess.entity.Transaction;
import com.netcracker.LMS.dataAccess.entity.models.BookAndMember;
import com.netcracker.LMS.dataAccess.entity.models.BookOfTransaction;
import com.netcracker.LMS.dataAccess.entity.models.MemberOfTransaction;
import com.netcracker.LMS.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequestMapping("/TransactionDb")
@RestController
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/addTransaction",method = RequestMethod.POST)
    public ResponseEntity<HttpStatus> addTransaction(@RequestBody Transaction transaction){
        HttpStatus status = null;
        try{
            transactionService.addTransaction(transaction);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/displayAllTransaction", method = RequestMethod.GET)
    public ResponseEntity<List> displayAllTransaction(){
        List<Transaction> transactionList = null;
        HttpStatus status = null;
        try{
            transactionList = transactionService.displayAllTransaction();
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(transactionList,status);
    }

    @RequestMapping(value = "/findTransaction", method = RequestMethod.GET)
    public ResponseEntity<Optional<Transaction>> findTransaction(@RequestParam UUID transactionId){
        HttpStatus status = null;
        Optional<Transaction> transactionList = null;
        try{
            transactionList = transactionService.findTransaction(transactionId);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(transactionList,status);
    }

    @RequestMapping(value = "/updateTransaction",method = RequestMethod.PUT)
    public ResponseEntity<HttpStatus> updateTransaction(@RequestParam UUID transactionId, @RequestBody Transaction transaction){
        HttpStatus status = null;
        try{
            transactionService.updateTransaction(transactionId,transaction);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/deleteTransaction", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteTransaction(@RequestParam UUID transactionId){
        HttpStatus status = null;
        try{
            transactionService.deleteTransaction(transactionId);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/memberDetails", method = RequestMethod.GET)
    public MemberOfTransaction memberDetails(@RequestParam UUID transactionId) throws JsonProcessingException {
        return transactionService.memberDetails(transactionId);
    }

    @RequestMapping(value = "/bookDetails", method = RequestMethod.GET)
    public BookOfTransaction bookDetails(@RequestParam UUID transactionId) throws JsonProcessingException {
        return transactionService.bookDetails(transactionId);
    }

    @RequestMapping(value = "/bookAndMemberDetails",method = RequestMethod.GET)
    public BookAndMember bookAndMemberDetails(@RequestParam UUID transactionId) throws JsonProcessingException{
        return transactionService.bookAndMemberDetails(transactionId);
    }

}
