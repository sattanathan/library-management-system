package com.netcracker.LMS.controller;

import com.netcracker.LMS.dataAccess.entity.Book;
import com.netcracker.LMS.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequestMapping("/BookDb")
@RestController
public class BookController {

    @Autowired
    BookService bookService;

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public ResponseEntity<HttpStatus> addBook(@RequestBody Book book){
        HttpStatus status = null;
        try{
            bookService.addBook(book);
            status = HttpStatus.CREATED;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/displayAllBooks", method = RequestMethod.GET)
    public ResponseEntity<List> displayAllBooks(){
        List<Book> bookList = null;
        HttpStatus status = null;
        try{
            bookList = bookService.displayAllBooks();
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(bookList,status);
    }

    @RequestMapping(value = "/findBook", method = RequestMethod.GET)
    public ResponseEntity<Optional<Book>> findBook(@RequestParam UUID bookId){
        HttpStatus status = null;
        Optional<Book> bookList = null;
        try{
            bookList = bookService.findBook(bookId);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(bookList,status);
    }


    @RequestMapping(value = "/updateBook",method = RequestMethod.PUT)
    public ResponseEntity<HttpStatus> updateBook(@RequestParam UUID bookId, @RequestBody Book book){
        HttpStatus status = null;
        try{
            bookService.updateBook(bookId,book);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/deleteBook", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteBook(@RequestParam UUID bookId){
        HttpStatus status = null;
        try{
            bookService.deleteBook(bookId);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/findBookByName", method = RequestMethod.GET)
    public List<Book> findBookByName(@RequestParam String bookTitle){
        return bookService.findBookByName(bookTitle);
    }

}
