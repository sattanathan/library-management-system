/*
package com.netcracker.LMS.controller;

import com.netcracker.LMS.dataAccess.entity.Publisher;
import com.netcracker.LMS.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping("/PublisherDb")
@RestController
public class PublisherController {
    @Autowired
    PublisherService publisherService;

    @RequestMapping(value = "/createPublisher",method = RequestMethod.POST)
    public ResponseEntity<?> createPublisher(@RequestBody Publisher p) {
        HttpStatus status = null;
        try{
            publisherService.createPublisher(p);
            status = HttpStatus.CREATED;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/displayAllPublisher", method = RequestMethod.GET)
    public ResponseEntity<List> displayAllPublisher(){
        List<Publisher> publisherList = null;
        HttpStatus status = null;
        try{
            publisherList = publisherService.displayAllPublisher();
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(publisherList,status);
    }

    @RequestMapping(value = "/findPublisher", method = RequestMethod.GET)
    public ResponseEntity<Publisher> findPublisher(@RequestParam UUID id){
        HttpStatus status = null;
        List<Publisher> publisherList = null;
        try{
            publisherList = publisherService.findPublisher(id);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/updatePublisher",method = RequestMethod.PUT)
    public ResponseEntity<HttpStatus> updateOper(@RequestParam UUID id, @RequestBody Publisher p){
        HttpStatus status = null;
        try{
            publisherService.updatePublisher(id,p);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/deletePublisher", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteEmployee(@RequestParam UUID id){
        HttpStatus status = null;
        try{
            publisherService.deletePublisher(id);
            status = HttpStatus.OK;
        }
        catch (Exception exception){
            status = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(status);
    }

}*/
