package com.netcracker.LMS.exceptions;

public class BookNotAvailable extends RuntimeException {
    public BookNotAvailable() {
        super("Book is not available");
    }
}
