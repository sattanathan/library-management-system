package com.netcracker.LMS.dataAccess.entity.models;

import com.netcracker.LMS.dataAccess.entity.Address;
import com.netcracker.LMS.dataAccess.entity.MemberType;
import com.netcracker.LMS.dataAccess.entity.Transaction;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class MemberOfTransaction {
    UUID memberId;
    String memberName;
    Address memberAddress;
    MemberType memberType;
    UUID transactionId;
    UUID bookId;
    String issueDate;
    String returnDate;
}
