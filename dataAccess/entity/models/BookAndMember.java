package com.netcracker.LMS.dataAccess.entity.models;

import com.netcracker.LMS.dataAccess.entity.Address;
import com.netcracker.LMS.dataAccess.entity.MemberType;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class BookAndMember {
    UUID transactionId;
    String issueDate;
    String returnDate;
    UUID bookId;
    String ISBN;
    String bookTitle;
    String author;
    UUID memberId;
    String memberName;
    Address memberAddress;
    MemberType memberType;
}
