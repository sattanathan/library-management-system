package com.netcracker.LMS.dataAccess.entity.models;

import com.netcracker.LMS.dataAccess.entity.Transaction;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class BookOfTransaction {
    UUID bookId;
    String ISBN;
    String bookTitle;
    String author;
    UUID transactionId;
    UUID memberId;
    String  issueDate;
    String returnDate;
}
