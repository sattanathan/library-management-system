package com.netcracker.LMS.dataAccess.entity;

public enum MemberType {
    gold(30),silver(15);

    public int getDays() {
        return days;
    }

    private  int days;

    MemberType(int days) {
        this.days=days;
    }
}
