package com.netcracker.LMS.dataAccess.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.UUID;

@Data
public class Transaction {
    @Id
    UUID transactionId;
    UUID memberId;
    UUID bookId;
    String returnDate;
    String issueDate;
}
