package com.netcracker.LMS.dataAccess.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.UUID;

@Data
public class Member {
    @Id
    UUID memberId;
    String memberName;
    Address memberAddress;
    MemberType memberType;
}
