package com.netcracker.LMS.dataAccess.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.UUID;

@Data
public class Book {
    @Id
    UUID bookId;
    String ISBN;
    String bookTitle;
    String author;
}

