package com.netcracker.LMS.dataAccess.entity;

import lombok.Data;

@Data
public class Address {
    int doorNumber;
    String street;
    String city;
    String State;
}
