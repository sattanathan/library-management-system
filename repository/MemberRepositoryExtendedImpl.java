package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class MemberRepositoryExtendedImpl implements MemberRepositoryExtended{

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Member> findByName(String memberName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("memberName").is(memberName));
        return mongoTemplate.find(query, Member.class);
    }
}
