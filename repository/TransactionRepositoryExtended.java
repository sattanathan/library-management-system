package com.netcracker.LMS.repository;

import java.util.UUID;

public interface TransactionRepositoryExtended {
    public Boolean findAvailability(UUID bookId);
}