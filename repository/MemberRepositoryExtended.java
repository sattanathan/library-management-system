package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Member;

import java.util.List;

public interface MemberRepositoryExtended {
    public List<Member> findByName(String memberName);
}
