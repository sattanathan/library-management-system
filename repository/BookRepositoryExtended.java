package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Book;

import java.util.List;

public interface BookRepositoryExtended {
    public List<Book> findByName(String bookName);
}
