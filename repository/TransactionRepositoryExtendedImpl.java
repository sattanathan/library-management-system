package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Transaction;
import com.netcracker.LMS.utils.CompareTransactionDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class TransactionRepositoryExtendedImpl implements TransactionRepositoryExtended {

    @Autowired
    MongoTemplate mongoTemplate;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    Calendar calendar;

    @Override
    public Boolean findAvailability(UUID bookId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("bookId").is(bookId));
        List<Transaction> list = mongoTemplate.find(query, Transaction.class);

        if(list.size() == 0)
            return true;

        list.sort(new CompareTransactionDate());
        System.out.println(list);
        if(list.get(0).getReturnDate().compareTo(sdf.format(calendar.getTime())) < 0){
            return true;
        }
        else return false;
    }

}