package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, UUID>, TransactionRepositoryExtended {
}
