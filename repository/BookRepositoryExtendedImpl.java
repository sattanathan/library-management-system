package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class BookRepositoryExtendedImpl implements BookRepositoryExtended {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Book> findByName(String bookTitle) {
        Query query = new Query();
        query.addCriteria(Criteria.where("bookTitle").is(bookTitle));
        return mongoTemplate.find(query, Book.class);
    }
}
