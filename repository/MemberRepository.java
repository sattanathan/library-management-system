package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Member;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MemberRepository extends MongoRepository<Member, UUID>,MemberRepositoryExtended {
}
