package com.netcracker.LMS.repository;

import com.netcracker.LMS.dataAccess.entity.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BookRepository extends MongoRepository<Book, UUID>, BookRepositoryExtended {
}
