package com.netcracker.LMS.utils;

import com.netcracker.LMS.dataAccess.entity.Transaction;

import java.util.Comparator;

public class CompareTransactionDate implements Comparator<Transaction> {

    @Override
    public int compare(Transaction o1, Transaction o2) {
       if(o1.getReturnDate().compareTo(o2.getReturnDate()) < 0)
           return -1;
       else
           return 1;
    }
}
